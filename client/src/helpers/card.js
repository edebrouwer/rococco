export default class Card {
    constructor(scene) {

        this.render = (x, y, sprite, face_down=false) => {

            if (face_down) {
              sprite = sprite + "_back";
              //scene.load.image(sprite, 'src/assets/red_back.png');
              scene.textures.renameTexture('cardBack',sprite);
            }
            let card = scene.add.image(x, y, sprite).setScale(0.3, 0.3).setInteractive();
            scene.input.setDraggable(card);

            var splt_string  = sprite.split('_');

            this.color = splt_string[1];
            this.value = splt_string[0];

            return card;
        }
    }
}
