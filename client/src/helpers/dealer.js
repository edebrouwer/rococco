import Card from './card'

export default class Dealer {
    constructor(scene) {
        this.dealCards = (cards_ids,first_hand_bool) => {

            var col_list = ["Hearts","Diamonds","Clovers","Spades"]
            var num_cards_up = cards_ids.length;

            for (let i = 0; i < num_cards_up; i++) {

                let playerCard = new Card(scene);

                var sprite_col = Math.floor(cards_ids[i]/13);
                var sprite_val = cards_ids[i]- (sprite_col*13) + 2
                var sprite_name = sprite_val.toString() + "_" + col_list[sprite_col];

                //console.log("Sprite Name " + sprite_name);
                scene.colors_in_hand[col_list[sprite_col]] = scene.colors_in_hand[col_list[sprite_col]]+1;

                if (first_hand_bool && (i==(num_cards_up-1))){
                scene.myCards.push(playerCard.render(475 + (i * 100), 750, sprite_name,true).disableInteractive());

                }
                else{
                scene.myCards.push(playerCard.render(475 + (i * 100), 750, sprite_name, false).disableInteractive());
                }

                //let opponentCard = new Card(scene);
                //scene.opponentCards.push(opponentCard.render(475 + (i * 100), 125, opponentSprite).disableInteractive());
            }

        }
    }
}
