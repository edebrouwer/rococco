import io from 'socket.io-client';
import Card from '../helpers/card';
import Dealer from "../helpers/dealer";
import Zone from '../helpers/zone';
import BiddingBut from '../helpers/bidding_but';
import update_scores from '../helpers/update_scores';

export default class Game extends Phaser.Scene {
    constructor() {
        super({
            key: 'Game'
        });
    }

    preload() {
        this.load.image('2_Diamonds', 'src/assets/2D.png');
        this.load.image('3_Diamonds', 'src/assets/3D.png');
        this.load.image('4_Diamonds', 'src/assets/4D.png');
        this.load.image('5_Diamonds', 'src/assets/5D.png');
        this.load.image('6_Diamonds', 'src/assets/6D.png');
        this.load.image('7_Diamonds', 'src/assets/7D.png');
        this.load.image('8_Diamonds', 'src/assets/8D.png');
        this.load.image('9_Diamonds', 'src/assets/9D.png');
        this.load.image('10_Diamonds', 'src/assets/10D.png');
        this.load.image('11_Diamonds', 'src/assets/JD.png');
        this.load.image('12_Diamonds', 'src/assets/QD.png');
        this.load.image('13_Diamonds', 'src/assets/KD.png');
        this.load.image('14_Diamonds', 'src/assets/AD.png');
        this.load.image('2_Hearts', 'src/assets/2H.png');
        this.load.image('3_Hearts', 'src/assets/3H.png');
        this.load.image('4_Hearts', 'src/assets/4H.png');
        this.load.image('5_Hearts', 'src/assets/5H.png');
        this.load.image('6_Hearts', 'src/assets/6H.png');
        this.load.image('7_Hearts', 'src/assets/7H.png');
        this.load.image('8_Hearts', 'src/assets/8H.png');
        this.load.image('9_Hearts', 'src/assets/9H.png');
        this.load.image('10_Hearts', 'src/assets/10H.png');
        this.load.image('11_Hearts', 'src/assets/JH.png');
        this.load.image('12_Hearts', 'src/assets/QH.png');
        this.load.image('13_Hearts', 'src/assets/KH.png');
        this.load.image('14_Hearts', 'src/assets/AH.png');
        this.load.image('2_Clovers', 'src/assets/2C.png');
        this.load.image('3_Clovers', 'src/assets/3C.png');
        this.load.image('4_Clovers', 'src/assets/4C.png');
        this.load.image('5_Clovers', 'src/assets/5C.png');
        this.load.image('6_Clovers', 'src/assets/6C.png');
        this.load.image('7_Clovers', 'src/assets/7C.png');
        this.load.image('8_Clovers', 'src/assets/8C.png');
        this.load.image('9_Clovers', 'src/assets/9C.png');
        this.load.image('10_Clovers', 'src/assets/10C.png');
        this.load.image('11_Clovers', 'src/assets/JC.png');
        this.load.image('12_Clovers', 'src/assets/QC.png');
        this.load.image('13_Clovers', 'src/assets/KC.png');
        this.load.image('14_Clovers', 'src/assets/AC.png');
        this.load.image('2_Spades', 'src/assets/2S.png');
        this.load.image('3_Spades', 'src/assets/3S.png');
        this.load.image('4_Spades', 'src/assets/4S.png');
        this.load.image('5_Spades', 'src/assets/5S.png');
        this.load.image('6_Spades', 'src/assets/6S.png');
        this.load.image('7_Spades', 'src/assets/7S.png');
        this.load.image('8_Spades', 'src/assets/8S.png');
        this.load.image('9_Spades', 'src/assets/9S.png');
        this.load.image('10_Spades', 'src/assets/10S.png');
        this.load.image('11_Spades', 'src/assets/JS.png');
        this.load.image('12_Spades', 'src/assets/QS.png');
        this.load.image('13_Spades', 'src/assets/KS.png');
        this.load.image('14_Spades', 'src/assets/AS.png');
        this.load.image('cardBack', 'src/assets/blue_back.png');
    }

    create() {
        this.isPlayerA = false;
        this.playerNumber = 0;
        this.opponentCards = [];
        this.myCards = [];
        this.tricktexts = [];
        this.card_hand = [];
        this.dropzoneCards = [];
        this.numCardsinHand = 0;

        this.zone = new Zone(this);
        this.dropZone = this.zone.renderZone();
        this.outline = this.zone.renderOutline(this.dropZone);

        this.dealer = new Dealer(this);

        this.bidding_but = new BiddingBut(this);

        this.colors_in_hand = {"Hearts":0,"Diamonds":0,"Clovers":0,"Spades":0};

        //this.current_scores_disp = new currentScoresDisp(this);
        this.score_board = {};

        this.bids = {};
        this.current_scores = {};
        this.total_scores = {};

        this.players_names_dict = {};

        this.first_round = false;

        this.player_name = "";

        this.rocaca_str = "";

        let self = this;

        this.socket = io('http://localhost:3000');

        this.socket.on('connect', function () {
            console.log('Connected!');
        });

        this.socket.on('isPlayerA', function () {
            self.isPlayerA = true;
        });

        this.disp_text = this.add.text(75, 500, ["Gotten cards  : " ]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#ff69b4');

        this.socket.on('newRound',function(total_scores){

          self.continue_txt.setText(" ");
          self.continue_txt.disableInteractive();

          self.status_txt.setText("Deal Cards for a new round !");
          self.dealText.setInteractive();

          self.rocaca_str = "";

          self.score_board =  update_scores(self, self.score_board, 1100, 250, self.players_names_dict, self.bids,self.current_scores, total_scores);
          self.total_scores = total_scores;

          for (var i =0; i<self.dropzoneCards.length;i++){
            self.dropzoneCards[i].destroy();
          }


        });

        this.socket.on('dealCards', function (cards_to_deal, players_array) {


          for (var i =0; i<self.myCards.length;i++){
            self.myCards[i].destroy();
          }

            self.myCards = [];

        self.colors_in_hand = {"Hearts":0,"Diamonds":0,"Clovers":0,"Spades":0};


            if (cards_to_deal[0].length==1){ //first round
              self.first_round = true;
              self.card_hand = [];
              for (var i = 0; i< players_array.length; i++){
                if (players_array[i] == self.playerNumber){
                  self.my_hidden_card =  cards_to_deal[i]; //card not to reveal
                }
                else{
                  self.card_hand = self.card_hand.concat(cards_to_deal[i]);
                }
              }
              self.card_hand = self.card_hand.concat(self.my_hidden_card);
            }

            else{ //other rounds
            self.first_round = false;
            for (var i = 0; i< players_array.length; i++){
              if (players_array[i] == self.playerNumber){
                self.card_hand = cards_to_deal[i];
              }
            }
            }

            //self.disp_text.setText("Hearts : " + self.colors_in_hand["Hearts"] + "Diamonds : " + self.colors_in_hand["Diamonds"] + "Clovers : " + self.colors_in_hand["Clovers"] + "Spades : " + self.colors_in_hand["Spades"])

            self.dealer.dealCards(self.card_hand,self.first_round);
            self.dealText.disableInteractive();

          //  self.status_txt.setText("Player "+ player_number + " to play");
          //  self.status_txt.setColor('#ff69b4');
          //  if (player_number == self.playerNumber){
          //    self.status_txt.setColor('#00ffff');
          //    for (var i = 0; i < self.myCards.length; i++) {
          //      console.log(self.myCards[i]);
          //      self.myCards[i].setInteractive();
          //      //Do something
          //      }
          //  }

            if (self.first_round){
              self.numCardsinHand = 1;
            }
            else{
            self.numCardsinHand = self.card_hand.length;
            }

            self.status_txt.setText("Cards are dealt ! Click to start bidding.");
            self.start_action.setInteractive();

        });


        this.socket.on('cardPlayed', function (gameObject, player_number, player_to_play, player_to_play_name, winner_player, atout_color,current_scores,first_color) {

            self.roundNumber_txt.setText(" ");

            if (player_number !== self.playerNumber) {
                //let sprite = gameObject.textureKey;
                var sprite_array = gameObject.textureKey.split('_');
                var sprite_reveal = sprite_array[0] + "_" + sprite_array[1];
                //self.opponentCards.shift().destroy();
                self.dropZone.data.values.cards++;
                let card = new Card(self);

                var c = card.render(((self.dropZone.x - 350) + (self.dropZone.data.values.cards * 50)), (self.dropZone.y), sprite_reveal).disableInteractive();
                self.dropzoneCards.push(c);

            }
            else{

              for (var i = 0; i < self.myCards.length; i++) {
                self.myCards[i].disableInteractive();
                //Do something
                }

            }

            if (atout_color != "white"){
            self.status_txt.setText(self.rocaca_str + "Player "+ player_to_play_name + " to play. Trump : " + atout_color);
            }
            else{
            self.status_txt.setText(self.rocaca_str + "Player "+ player_to_play_name + " to play");
            }
            self.status_txt.setColor('#ff69b4');
            //self.disp_text.setText("Hearts : " + self.colors_in_hand["Hearts"] + "Diamonds : " + self.colors_in_hand["Diamonds"] + "Clovers : " + self.colors_in_hand["Clovers"] + "Spades : " + self.colors_in_hand["Spades"])
            if (player_to_play == self.playerNumber){
              self.status_txt.setColor('#00ffff');

              if (self.first_round){
                self.myCards[self.myCards.length-1].setInteractive();
              }

              else{
              for (var i = 0; i < self.myCards.length; i++) {
                console.log(self.myCards[i]);
                if ((self.myCards[i].texture.key.split('_')[1]==first_color) || (self.colors_in_hand[first_color]==0)){
                self.myCards[i].setInteractive();
                }
                }
              }

            }

            if (player_to_play == 0){
              self.status_txt.setText("Trick is over ! --  " + winner_player + " wins !");

              self.score_board =  update_scores(self, self.score_board, 1100, 250, self.players_names_dict, self.bids,current_scores, self.total_scores);

              self.continue_txt.setText("Click to Continue");
              self.continue_txt.setInteractive();
            }

        });

        this.socket.on("Update_connected_players",function(players_names_dict,bids,current_scores, total_scores){
          //self.score_board = {};
          self.score_board =  update_scores(self, self.score_board, 1100, 250, players_names_dict, bids,current_scores,total_scores);
          self.players_names_dict = players_names_dict;
          self.bids = bids;
          //self.players_names_dict = players_names_dict
        });

        this.socket.on("assignNumberToPlayer",function(n_player){


          if (self.playerNumber == 0){
            self.player_name = prompt("Please enter your name", "name") ;
            self.playerNumber = n_player;
            self.your_identity.setText("You are "+ self.player_name) ;
            self.socket.emit("setName", self.player_name, self.playerNumber) ;
          }


        self.tricktexts = [self.bidding_but.render(800,100,0,self.playerNumber), self.bidding_but.render(900,100,1,self.playerNumber), self.bidding_but.render(1000,100,2,self.playerNumber),
                            self.bidding_but.render(800,150,3,self.playerNumber),self.bidding_but.render(900,150,4,self.playerNumber),self.bidding_but.render(1000,150,5,self.playerNumber)];

        });


        this.socket.on("gros_caca_check",function(player_to_shit, player_to_shit_name){
            self.start_action.disableInteractive();
            self.continue_txt.setText(" ");
            self.continue_txt.disableInteractive();

            self.status_txt.setText("Waiting for "+ player_to_shit_name + " to decide the game type.");


            if (self.playerNumber == player_to_shit){
              self.roundNumber_txt.setText("Do you want this to be un GROS CACA ?");
              self.gros_caca_oui.setText("YES");
              self.gros_caca_oui.setInteractive();

              self.gros_caca_non.setText("Hell NO !");
              self.gros_caca_non.setInteractive();

            }
            else{
              self.roundNumber_txt.setText("On attend de voir si c'est un gros caca.");
            }

        });

        this.socket.on("bidding_session_started",function(player_to_bid,player_to_bid_name,players_names_dict,cards_per_hand,rocaca_trump){
            self.start_action.disableInteractive();
            self.continue_txt.setText(" ");
            self.continue_txt.disableInteractive();

            self.gros_caca_oui.setText("");
            self.gros_caca_oui.disableInteractive();

            self.gros_caca_non.setText("");
            self.gros_caca_non.disableInteractive();

            //self.score_board =  update_scores(self, self.score_board, 1100, 250, self.bids,self.current_scores, total_scores);
            //self.total_scores = total_scores;


            //setTimeout(function(){self.status_txt.setText(win_text);}, 2000);

            //for (var i =0; i<self.dropzoneCards.length;i++){
            //  self.dropzoneCards[i].destroy();

            //}

            var leading_text = "";
            self.rocaca_str = "";
            if (rocaca_trump != "white"){
              self.rocaca_str = "ROCACA ("+ rocaca_trump +") - ";
              leading_text = "ROCACA TIME ! Atout: " + rocaca_trump+ ".  ";
            }

            self.status_txt.setText("Bidding session started. Waiting for "+ player_to_bid_name + " to bid.");
            self.roundNumber_txt.setText(leading_text + "Bidding for new round ! " + cards_per_hand + " card(s) dealt.");

            if (self.playerNumber == player_to_bid){
                for (var i =0; i<self.tricktexts.length; i++){
                  self.tricktexts[i].setInteractive();
                }
            }

            self.players_names_dict = players_names_dict;

        });

        this.socket.on("next_bid", function(player_to_bid,player_to_bid_name,bids,current_scores,total_scores,players_names_dict,last_bidder,non_valid_bid){

          //self.current_scores_list.destroy();
          self.bids = bids;
          self.total_scores = total_scores;
          self.current_scores = current_scores;

          //self.current_scores_disp.render(1100,250,bids,current_scores, total_scores);

          self.score_board =  update_scores(self, self.score_board, 1100, 250, players_names_dict, bids,current_scores, total_scores);
          //self.players_names_dict = players_names_dict

          self.status_txt.setText("Bidding session started. Waiting for "+ player_to_bid_name + " to bid.");
          for (var i =0; i<self.tricktexts.length; i++){
            self.tricktexts[i].disableInteractive();
          }

          if (self.playerNumber == player_to_bid){
              for (var i =0; i<self.tricktexts.length; i++){
                self.tricktexts[i].setInteractive();
              }
          }

          if (last_bidder){
            self.tricktexts[non_valid_bid].disableInteractive();
          }

        })

        this.socket.on("end_bid",function(bids, current_scores,total_scores, player_number, player_number_name, players_names_dict){

          self.bids = bids;
          self.total_scores = total_scores;
          self.current_scores = current_scores;
          self.players_names_dict = players_names_dict;


          //self.scores_table = self.current_scores_update.update(1100,250,bids,current_scores,total_scores);
          self.score_board =  update_scores(self, self.score_board, 1100, 250, players_names_dict, bids,current_scores, total_scores);


          for (var i =0; i<self.tricktexts.length; i++){
            self.tricktexts[i].disableInteractive();
          }

            self.roundNumber_txt.setText(" ");
            //self.status_txt.setText("Bidding session complete. Waiting for dealing cards ...");
            //self.dealText.setInteractive();


            self.status_txt.setText(self.rocaca_str + player_number_name + " to play");
            self.status_txt.setColor('#ff69b4');
            if (player_number == self.playerNumber){
              self.status_txt.setColor('#00ffff');
              if (self.first_round){
                self.myCards[self.myCards.length-1].setInteractive();
              }
              else{
              for (var i = 0; i < self.myCards.length; i++) {
                console.log(self.myCards[i]);
                self.myCards[i].setInteractive();
                //Do something
                }
              }
            }

            if (self.rocaca_str==""){
            self.roundNumber_txt.setText("BON CHANCE !!!");
          }
          else{
            self.roundNumber_txt.setText("BONNE MERDE !!!");
          }


        });

        this.socket.on("startTrick",function(player_number, player_number_name){

          self.dropZone.data.values.cards = 0;

          self.continue_txt.setText(" ");
          self.continue_txt.disableInteractive();

          for (var i =0; i<self.dropzoneCards.length;i++){
            self.dropzoneCards[i].destroy();
          }

          self.status_txt.setText(self.rocaca_str + player_number_name + " to play");
          self.status_txt.setColor('#ff69b4');
          if (player_number == self.playerNumber){
            self.status_txt.setColor('#00ffff');
            if (self.first_round){
              self.myCards[self.myCards.length-1].setInteractive();
            }
            else{
            for (var i = 0; i < self.myCards.length; i++) {
              console.log(self.myCards[i]);
              self.myCards[i].setInteractive();
              //Do something
              }
            }
          }

        });

        this.title_test = this.add.text(75, 75, ["ROCOCCONLINE (beta)"]).setFontSize(28).setFontFamily('Trebuchet MS').setColor('#ff69b4');
        this.status_txt = this.add.text(75, 150, "Waiting for next round ...").setFontSize(18).setFontFamily('Trebuchet MS').setColor('#ff69b4');
        this.dealText = this.add.text(75, 350, ['DEAL CARDS']).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff').setInteractive();
        this.your_identity = this.add.text(500,75, ["You are no one"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
        this.start_action = this.add.text(75, 700, ['START BIDDING']).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');//.setInteractive();
        this.continue_txt = this.add.text(1100, 700, [' ']).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');

        self.roundNumber_txt = this.add.text(400, 350, [' ']).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');

        this.numberTricks_text = this.add.text(850,50,["How many tricks ?"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');

        this.scoresText = this.add.text(1225,200,["Scores"]).setFontSize(22).setFontFamily('Trebuchet MS').setColor('#ff69b4');
        this.scoresText2 = this.add.text(1100,225,["Player      Bid  Current  Total"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');

        this.gros_caca_oui = this.add.text(850,350,[""]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
        this.gros_caca_non = this.add.text(950,350,[""]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');


        //this.zero_trick_text = this.bidding_but.render(900,150,0);
        //this.one_trick_text  = this.bidding_but.render(1000,150,1);
        //this.zerotrick_text = this.add.text(900,150,["Zero trick"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
        //this.onetrick_text = this.add.text(1000,150,["One trick"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
        //this.twotrick_text = this.add.text(1100,150,["Two tricks"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');


        this.dealText.on('pointerdown', function () {
            self.socket.emit("dealCards");
        })

        this.dealText.on('pointerover', function () {
            self.dealText.setColor('#ff69b4');
        })

        this.dealText.on('pointerout', function () {
            self.dealText.setColor('#00ffff');
        })

        this.start_action.on('pointerdown', function () {
            self.socket.emit("gros_caca");
        })

        this.start_action.on('pointerover', function () {
            self.start_action.setColor('#ff69b4');
        })

        this.start_action.on('pointerout', function () {
            self.start_action.setColor('#00ffff');
        })

        this.continue_txt.on('pointerdown', function () {
            self.socket.emit("newTrick",self.numCardsinHand);
        })

        this.continue_txt.on('pointerover', function () {
            self.continue_txt.setColor('#ff69b4');
        })

        this.continue_txt.on('pointerout', function () {
            self.continue_txt.setColor('#00ffff');
        })

        this.gros_caca_oui.on('pointerdown', function () {
            self.socket.emit("start_bidding",1);
        })

        this.gros_caca_oui.on('pointerover', function () {
            self.gros_caca_oui.setColor('#ff69b4');
        })

        this.gros_caca_oui.on('pointerout', function () {
            self.gros_caca_oui.setColor('#00ffff');
        })

        this.gros_caca_non.on('pointerdown', function () {
            self.socket.emit("start_bidding",0);
        })

        this.gros_caca_non.on('pointerover', function () {
            self.gros_caca_non.setColor('#ff69b4');
        })

        this.gros_caca_non.on('pointerout', function () {
            self.gros_caca_non.setColor('#00ffff');
        })


        this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
            gameObject.x = dragX;
            gameObject.y = dragY;
        })

        this.input.on('dragstart', function (pointer, gameObject) {
            gameObject.setTint(0xff69b4);
            self.children.bringToTop(gameObject);
        })

        this.input.on('dragend', function (pointer, gameObject, dropped) {
            gameObject.setTint();
            if (!dropped) {
                gameObject.x = gameObject.input.dragStartX;
                gameObject.y = gameObject.input.dragStartY;
            }
        })

        this.input.on('drop', function (pointer, gameObject, dropZone) {
            dropZone.data.values.cards++;

            gameObject.x = (dropZone.x - 350) + (dropZone.data.values.cards * 50);
            gameObject.y = dropZone.y;

            self.myCards = self.myCards.filter(card => card !== gameObject);
            self.numCardsinHand = self.numCardsinHand -1;

            var s_array = gameObject.texture.key.split('_');

            if (self.first_round){

              var sprite_reveal = s_array[0] + "_" + s_array[1];

              self.textures.setTexture(gameObject,sprite_reveal);
            }

            self.colors_in_hand[s_array[1]] = self.colors_in_hand[s_array[1]]-1; //drop this color by 1.

            self.dropzoneCards.push(gameObject);
            gameObject.disableInteractive();
            self.socket.emit('cardPlayed', gameObject, self.playerNumber);


        })
    }

    update() {

    }
}
