export default class BiddingBut {
    constructor(scene) {

        this.render = (x, y, value, player_number) => {

            let but = scene.add.text(x,y,[value + " trick(s)"]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');

            but.on('pointerover', function () {
                but.setColor('#ff69b4');
            })

            but.on('pointerout', function () {
                but.setColor('#00ffff');
            })

            but.on('pointerdown', function () {
                scene.socket.emit("bidded",value, player_number);
            })

            return but;
        }
    }
}
