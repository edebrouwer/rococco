export default function update_scores(scene, scores_board,x,y,players_name_dict,bids,scores,total_scores){

    var bids_keys = Object.keys(bids);
    var scores_keys = Object.keys(scores_board);

    var player_array = bids_keys.concat(scores_keys);

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    };

    var player_array_unique = player_array.filter( onlyUnique );

    for(var i_p = 0; i_p<player_array_unique.length; i_p++){

      var player = player_array_unique[i_p];

      if (player in bids){

        var player_name = players_name_dict[player];
        console.log("Player name : "+player_name + "Number : " + player);
        var player_name_l = player_name.length;

        if (player_name_l > 10){
        player_name = player_name.substring(0,10);
        }

        player_name_l = player_name.length;
        //var trailing_blank = ' '.repeat(13-player_name_l);

        console.log("Player NUmber score : "+player);

      if(!(player in scores_board)){
        console.log("Creating new player entry in scores board");
        var i = Object.keys(scores_board).length;
        scores_board[player] = [scene.add.text(x-20,y+i*20,[ player_name ]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff'),scene.add.text(x+90,y+i*20,[bids[player]+"        "+scores[player]+"           "+total_scores[player]]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff')];
      }
      else{
        console.log("Updating scores board");
        scores_board[player][0].setText( player_name );
        scores_board[player][1].setText(bids[player]+"        "+scores[player]+"           "+total_scores[player]);
      }

    }
    else{
       console.log("Deleting entry in scores board : "+player);
      delete scores_board[player];
    }
  }

    return scores_board;
  };
