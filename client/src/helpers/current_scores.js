export default class currentScoresDisp {
    constructor(scene) {

        this.but_dict = {};
        this.i = 0;

        this.render = (x, y, bids, scores, total_scores) => {

            for(var player in bids){
              this.but_dict[player] = scene.add.text(x,y+self.i*20,["Player "+ player + "     " + bids[player]+"        "+scores[player]+"           "+total_scores[player]]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
              self.i ++;
            }

            return this.but_dict;
        };

        this.update = (x, y, bids, scores,total_scores) => {

          for(var player in bids){

            if(!(player in this.but_dict)){
              this.but_dict[player] = scene.add.text(x,y+self.i*20,["Player "+ player + "     " + bids[player]+"        "+scores[player]+"           "+total_scores[player]]).setFontSize(18).setFontFamily('Trebuchet MS').setColor('#00ffff');
              self.i ++;
            }
            else{
              this.but_dict[player].setText("Player "+ player + "     " + bids[player]+"        "+scores[player]+"           "+total_scores[player]);
            }

          }
          return this.but_dict;
        };
    }
}
