const server = require('express')();
const http = require('http').createServer(server);
const io = require('socket.io')(http);
let players = [];
let n_current = 0;
let players_numbers = [];
let idx_player_to_play = 0;
let number_players_played =  0 ;
let numbers_id_dict = {};
let id_numbers_dict = {};
let players_num_name_dict = {};
let player_to_play = 0;
let player_to_bid = 0;
let winner_player  = 0;
let atout_color = "white";
let first_color = "white";
let highest_val = 0;
let card_number = 52;
let cards_array = Array.from(Array(card_number).keys());
let cards_per_hand = 1;
let card_increment = 1;
let bids = {};
let scores = {};
let current_scores = {};
let total_scores = {};
let winner_index = 0;
let gros_caca_bool = false;
let colors_array = ["Hearts","Diamonds","Clovers","Spades"];
let players_this_round = [];

let win_points = 2;
let plus_trick = 1;
let minus_trick = -1;


function shuffle(a) {
var j, x, i;
for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
}
return a;
}

io.on('connection', function (socket) {
    console.log('A user connected: ' + socket.id);

    players.push(socket.id);

    if (players.length === 1) {
        io.emit('isPlayerA');
    };

    n_current = n_current + 1;
    console.log("N_current : " + n_current);
    numbers_id_dict[socket.id] = n_current;
    id_numbers_dict[n_current] = socket.id;

    current_scores[n_current] = 0;
    total_scores[n_current]   = 0;
    bids[n_current]   = 0;
    //players_num_name_dict[n_current] = "You";


    players_numbers.push(n_current);
    io.emit("assignNumberToPlayer",n_current);


    socket.on('dealCards', function () {
        idx_player_to_play = winner_index;
        atout_color = "white";
        first_color = "white";
        highest_val = 0;

        console.log(cards_array);
        console.log(shuffle(cards_array));

        cards_array = shuffle(cards_array);

        var cards_to_deal = new Array(players.length);

        players_this_round = [];
        bids = {};
        for (var i = 0; i< players.length; i++){
          players_this_round.push(players_numbers[i]);
          bids[players_numbers[i]] = 0;
          current_scores[players_numbers[i]] = 0; // reset number of tricks won.
          cards_to_deal[i] = cards_array.slice(i*cards_per_hand, (i+1)*cards_per_hand);
        }

        if (winner_index >= players_this_round.length){ //if a user quitted.
          winner_index = 0
        }

        console.log("Cards to deal " + cards_to_deal);
        console.log("Players Numbers " + players_numbers);


        io.emit('dealCards',cards_to_deal, players_numbers);
    });

    socket.on("setName",function(player_name,playerNumber){
      if (player_name == undefined){ //remove the player from the list
        var number_to_remove = playerNumber;
        var socket_to_remove = id_numbers_dict[playerNumber];

        players = players.filter(player => player !== socket_to_remove);
        players_numbers = players_numbers.filter(player => player !== number_to_remove);
        delete players_num_name_dict[number_to_remove];
        delete bids[number_to_remove];
        console.log("Removed player " + number_to_remove + " from the pool")
      }
      else{
      players_num_name_dict[playerNumber] = player_name;

      console.log("Name set : " + playerNumber + " Name : " + players_num_name_dict[playerNumber]);
      io.emit("Update_connected_players",players_num_name_dict,bids, current_scores, total_scores);
      }
    });

    socket.on('cardPlayed', function (gameObject, playerNumber) {
        console.log('Card Played :' + gameObject.textureKey);

        var splt_string  = gameObject.textureKey.split('_');

        var color = splt_string[1];
        var value = parseInt(splt_string[0]);

        if (number_players_played == 0){
          first_color = color;
        }

        if (atout_color != "white"){
            if (color == atout_color){
              if (value > highest_val){
                highest_val = value;
                winner_player = playerNumber;
              }
            }
        }
        else{
          if(color==first_color){
            if(value > highest_val){
              highest_val = value;
              winner_player = playerNumber;
            }
          }
          else{
            if (gros_caca_trump != color){ // dealing for rocaca case.
            atout_color = color;
            highest_val = value;
            winner_player = playerNumber;
            }
          }
        }


        idx_player_to_play = (idx_player_to_play+1) % players_this_round.length;
        number_players_played = number_players_played + 1;

        if (number_players_played > players_this_round.length-1){
          current_scores[winner_player] = current_scores[winner_player]+1;
          player_to_play = 0;
          winner_index = players_numbers.findIndex(function checkWinner(playNum) {return playNum==winner_player;});
        }
        else{ player_to_play = players_this_round[idx_player_to_play]}
        io.emit('cardPlayed', gameObject, playerNumber, player_to_play, players_num_name_dict[player_to_play], players_num_name_dict[winner_player], atout_color, current_scores,first_color);

    });

    socket.on('gros_caca', function () {
        console.log('Gros caca time:');
        idx_player_to_shit = winner_index -1;
        if (idx_player_to_shit < 0){
          idx_player_to_shit = players_this_round.length - 1;
        }
        io.emit('gros_caca_check',players_this_round[idx_player_to_shit],players_num_name_dict[players_this_round[idx_player_to_shit]]);
    });


    socket.on('start_bidding', function (yes) {
        console.log('Bidding Session Started :');
        gros_caca_bool = false;
        gros_caca_trump = "white";
        if (yes){
          console.log("Gros Caca");
          gros_caca_bool = true;
          gros_caca_trump = shuffle(colors_array)[0];
          console.log("Caca trump : "+ gros_caca_trump);
        }
        idx_player_to_play = winner_index;
        number_players_played = 0;
        for (p in bids){
          bids[p] = 0; //reset bids
        }

        console.log("Player to bid  : "+ players_this_round[idx_player_to_play]);
        console.log("Player name : " + players_num_name_dict[players_this_round[idx_player_to_play]]);

        for (p in players_num_name_dict)
        {
          console.log("Players names dict : " + p + "  " +players_num_name_dict[p]);
        }

        io.emit('bidding_session_started',players_this_round[idx_player_to_play],players_num_name_dict[players_this_round[idx_player_to_play]], players_num_name_dict, cards_per_hand, gros_caca_trump);
    });

    socket.on('bidded', function (bid_value,player_number) {
        console.log('Player  :'+ player_number + " bidded with value : " + bid_value);
        bids[player_number] = bid_value;

        idx_player_to_play = (idx_player_to_play + 1) % players_this_round.length;
        number_players_played = number_players_played + 1;

        console.log("Player index to play :" + idx_player_to_play);
        console.log("Players numbers : " + players_numbers);
        if (number_players_played > players_this_round.length-1){ //end bidding session.
          console.log("End of bidding session");
          idx_player_to_play = winner_index;
          number_players_played = 0;
          first_player_to_play = players_this_round[idx_player_to_play];
          io.emit('end_bid', bids, current_scores, total_scores, first_player_to_play, players_num_name_dict[first_player_to_play], players_num_name_dict);
        }
        else{
        player_to_bid = players_this_round[idx_player_to_play];

        last_bidder_bool = (number_players_played == (players_this_round.length-1)); //this is the last bidder
        var sum_bids = 0;
        for (bid_num in bids){
          sum_bids = sum_bids + bids[bid_num];
        }
        non_valid_bid_index = cards_per_hand - sum_bids;
        if (sum_bids<0){
          last_bidder_bool = false;
        }
        console.log("Player to bid : " + player_to_bid);
        console.log("Player to bid : " + players_num_name_dict[player_to_bid]);
        io.emit('next_bid', player_to_bid, players_num_name_dict[player_to_bid], bids,current_scores,total_scores,players_num_name_dict,last_bidder_bool, non_valid_bid_index);
      }
    });

    socket.on("newTrick",function(num_cards){

      idx_player_to_play = winner_index;
      number_players_played = 0;

      atout_color = "white";
      first_color = "white";
      highest_val = 0;

      if (num_cards >0){
      io.emit("startTrick",players_this_round[idx_player_to_play],players_num_name_dict[players_this_round[idx_player_to_play]]);
    }
      else{ // round is over !
        //idx_player_to_play = 0;
        // increment the number of cards to deal.
        winner_index = cards_per_hand  % players_this_round.length; //give hand to next player in the circle

        if ((cards_per_hand+card_increment)>(card_number/players_this_round.length)){
          card_increment = -1
        }
        cards_per_hand = cards_per_hand + card_increment;

        // compute scores.
        console.log(players_numbers);
        console.log(current_scores);
        console.log(bids);
        console.log(total_scores);

        for (var i =0; i< players_this_round.length;i++)
        {
          player = players_this_round[i];
          console.log(player);
          if (bids[player]!=current_scores[player]){ //lost
          console.log(total_scores[player]);
          total_scores[player] = total_scores[player] + Math.abs(bids[player]-current_scores[player])*minus_trick;
          console.log(total_scores[player]);
        }
        else{ //win
          console.log("Winning player : " + player)
          console.log(total_scores[player]);
          total_scores[player] = total_scores[player] + bids[player]*plus_trick + win_points;
          console.log(total_scores[player]);
        }
        console.log('Total Scores each : ' + total_scores[player]);
        }
        console.log('Bidding Session Started :');

        for (k in total_scores){
          console.log('Total Scores : ' + total_scores[k]);
        }
        io.emit('newRound',total_scores);
      }
    });

    socket.on('disconnect', function () {
        var number_to_remove = numbers_id_dict[socket.id];
        console.log('A user disconnected: ' + socket.id + " with number : " + number_to_remove);
        players = players.filter(player => player !== socket.id);
        players_numbers = players_numbers.filter(player => player !== number_to_remove);
        players_this_round = players_this_round.filter(player => player !== number_to_remove);

        delete players_num_name_dict[number_to_remove];
        delete bids[number_to_remove];
        //io.emit("Update_connected_players",players_num_name_dict,bids, current_scores, total_scores);
        console.log("Number of players in the pool : " + players_numbers.length);
        console.log("Number of players in the pool : " + players.length);
    });
});

http.listen(3000, function () {
    console.log('Server started!');
});
